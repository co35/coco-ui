import request from '@/utils/request'
import { parseStrEmpty } from "@/utils/ruoyi";
// 分片上传
export function uploadChunk(data) {
  return request({
    url: '/file/uploadChunk',
    headers: { 'Content-Type': 'multipart/form-data' },
    method: 'post',
    data: data
  })
}
// 检查分片是否已上传
export function checkFile(query) {
  return request({
    url: '/file/checkFile',
    method: 'get',
    params: query
  })
}


export function mergeFile(data) {
  return request({
    url: '/file/mergeFile',
    method: 'post',
    data: data
  })
}